import random


def add_modifier(dict, key, value):
    # print(dict),
    # print(key),
    # print(value),
    if key in dict:
        dict[key] += value
    else:
        dict[key] = value


def cap(text):
    text[0] = text[0].capitalize()
    return text
