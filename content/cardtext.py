class Term:
    def __init__(self, text):
        self.n = text
        self.g = text
        self.d = text
        self.a = text
        self.i = text
        self.m = text
        self.v = text
        # mianownik, dopełniacz, celownik, biernik, narzędnik, miejscownik (!), wołacz
        # nominativus, genitivus, dativus, accusativus, instrumentativus, locativus (!), vocativus

    def __repr__(self):
        return repr(self.n)


CARDS = Term(["",
              "1 karta",
              "2 karty",
              "3 karty",
              "4 karty",
              "5 kart",
              "6 kart",
              "7 kart",
              "8 kart",
              "9 kart",
              "10 kart",
              "11 kart",
              "12 kart",
              "13 kart",
              "14 kart",
              "15 kart"
              ])

CARDS.d = Term(["ERROR",
                "1 kartę",
                "2 karty",
                "3 karty",
                "4 karty",
                "5 kart",
                "6 kart",
                "7 kart",
                "8 kart",
                "9 kart",
                "10 kart",
                "11 kart",
                "12 kart",
                "13 kart",
                "14 kart",
                "15 kart"
                ])


YOU_DISCARD = Term("odrzuć")

DESTINATION = Term("Cel")

NATURE = Term("Natura")

MON = Term("Stwór")
MON.g = "Stwora"

SUPPORT = Term("Wsparcie")
SUPPORT.g = "Wsparcia"

WAR = Term("Technologiczny")
WAR.g = "Technologicznego"

FIRE = Term("Lądowy")
FIRE.g = "Lądowego"

WATER = Term("Wodny")
WATER.g = "Wodnego"

AIR = Term("Powietrzny")
AIR.g = "Powietrznego"

GOOD = Term("Dobry")
GOOD.g = "Dobrego"

EVIL = Term("Zły")
EVIL.g = "Złego"



# nat = CARDS
# print(nat)
# print(nat.n)
# print(nat.g)
# print(nat.d)
# print(nat.a)
# print(nat.i)
# print(nat.m)
# print(nat.v)
