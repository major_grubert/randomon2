class Card:
    def __init__(self, cardset, cardid):
        self.cardset = cardset
        self.cardid = cardid
        self.cardset_name = cardset.name
        self.cardtype = "undefined"
        self.valid = True
        self.tags = []

    @property
    def show_debug(self):
        return self.__dict__
