import string

from utils.utils import *
from cards.card import Card
from cards.mons.mon import Mon


def random_name(size=3, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class Cardset:
    def __init__(self, how_many_cards, name=random_name()):
        self.cards = []
        self.name = name.lower()
        random.seed(self.name)

        self.populate_with_mons(how_many_cards)

    def populate(self, how_many_cards):
        for i in range(0, how_many_cards):
            self.cards.append(Card(self, i))

    def populate_with_mons(self, how_many_cards):
        for i in range(0, how_many_cards):
            self.cards.append(Mon("no_level", self, i))

    def display_debug(self):
        for card in self.cards:
            print(card.show_debug)

    def mons(self):
        mons = []
        for card in self.cards:
            if card.cardtype == "mon":
                mons.append(card)
        return mons

    def mons_of_level(self, level):
        mons = []
        for mon in self.mons():
            if mon.level == level:
                mons.append(mon)
        return mons

