from cards.card import Card
from cards.mons.evolutions import Evolutions
from cards.mons.random_level import random_level
from utils.utils import *
from utils.presets import *


class Mon(Card):
    def __init__(self, level="no_level", *args):
        super().__init__(*args)
        self.cardtype = "mon"
        self.level = level
        self.natures = []
        self.abilities = []
        self.attack_modifiers = {}
        self.energy_modifiers = {}
        self.evolutions = Evolutions()
        self.battletype = "not set"
        self.evolutions_sources_text = self.evolutions.sources_text()

        if self.level == "no_level":
            self.level = random_level(self.cardset)

        self.attack = 1 + (self.level * 2)
        self.energy = 0
        self.resistance = False

        self.apply_battletype()


    def apply_battletype(self):
        self.battletype = random.choice(["war", "mid", "counter"])
        if self.battletype == "war":
            self.energy += 10
        elif self.battletype == "mid":
            self.attack += 6
        else:
            self.attack += 3
            self.resistance = True

    @property
    def show_debug(self):
        return [self.__dict__, self.evolutions.show_debug]

