from content.cardtext import *


class Evolutions:
    def __init__(self):
        self.targets = []
        self.processes = []

    def set_process(self, num, pattern, sources, cost):
        self.processes[num] = Evolution(pattern, sources, cost)

    def sources(self):
        output = []
        if self.processes:
            for process in self.processes:
                if process:
                    for source in process.sources:
                        output.append(source)
        return output

    def sources_text(self):
        output = []
        for process in self.processes:
            output.append(process.text)
        return output

    @property
    def show_debug(self):
        output = [self.__dict__]
        if self.processes:
            for process in self.processes:
                if process:
                    for source in process.sources:
                        output.append(source.show_debug)
        return output


class Evolution:
    def __init__(self, pattern = "", sources = [], cost = ""):
        self.pattern = pattern
        self.sources = sources
        self.cost = cost

    def text(self):
        if self.pattern == "discard":
            return str(self.sources[0].cardid + ": " + CARDS_N[self.cost])
        elif self.pattern == "specific":
            return str(self.sources[0].cardid + ": " + self.cost)
        elif self.pattern == "fusion":
            return str(self.sources[0].cardid + self.sources[1])
        else:
            return str(self.cost)

    @property
    def show_debug(self):
        return self.__dict__
