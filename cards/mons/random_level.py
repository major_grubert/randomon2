from utils.utils import *


def random_level(cardset):
    threshold = get_orphan_threshold(cardset)
    for i in [0, 1, 2]:
        if over_x_level_y_orphans_in(threshold, i, cardset):
            return i+1
    die = random.randint(1, 6)
    if die in [1, 2]:
        return 0
    elif die in [3, 4]:
        return 1
    elif die in [5]:
        return 2
    else:
        return 3


def get_orphan_threshold(cardset):
    return len(cardset.cards)//20


def over_x_level_y_orphans_in(x, y, cardset):
    count = 0
    for mon in cardset.mons_of_level(y):
        if len(mon.evolutions.targets) == 0:
            count += 1
    if count > x:
        return True
    else:
        return False
